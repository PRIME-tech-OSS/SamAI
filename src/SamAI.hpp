#ifndef SAM_AI_HPP
#define SAM_AI_HPP

#include "Dense.hpp"
#include "Relu.hpp"
#include "DataLoader.hpp"
#include "ShaderGenerator.hpp"
#include "Trainer.hpp"

#endif // SAM_UTILITY_HPP
