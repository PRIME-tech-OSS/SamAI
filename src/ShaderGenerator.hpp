#ifndef SAM_SHADER_GENERATOR_HPP
#define SAM_SHADER_GENERATOR_HPP

#include <string>
#include "Network.hpp"
#include "Utility.hpp"

namespace sam
{

template<class Network>
class ShaderGenerator {
  Network m_network;
public:
  ShaderGenerator(const Network& network);
  std::string run();
  std::string genConstantVariable();
  std::string genBindingBuffer();
  std::string genPushConstant();
  std::string genMain();
};

template<class Network>
ShaderGenerator<Network>::ShaderGenerator(const Network& network) {
  m_network = network;
}

template<class Network>
std::string ShaderGenerator<Network>::run() {
  std::ostringstream oss;
  oss << "#version 450" << '\n';
  oss << "layout (local_size_x = " << m_network.getBatchSize();
  oss << ", local_size_y = 1, local_size_z = 1) in;\n";
  oss << genConstantVariable();
  oss << genBindingBuffer();
  oss << genPushConstant();
  oss << genMain();
  return oss.str();
}

template<class Network>
std::string ShaderGenerator<Network>::genConstantVariable() {
  std::ostringstream oss;
  const auto add_const_int = [&] (const std::string name, const int value) {
    oss << "const int " << name << " = " << value << ";\n";
  };
  oss << '\n';
  add_const_int("num_data", m_network.getBatchSize());
  add_const_int("num_invocation", m_network.getBatchSize());
  int offsetNetwork = 0;
  int offsetLayer = 0;
  for (const auto& layer: m_network.m_layers) {
    if (!layer->isActivation()) {
      std::string layerIndex = std::to_string(layer->index());
      add_const_int("width_layer_" + layerIndex, layer->width());
      add_const_int("height_layer_" + layerIndex, layer->height());
      add_const_int("offset_weight_" + layerIndex, offsetNetwork);
      offsetNetwork += layer->width() * layer->height();
      add_const_int("offset_bias_" + layerIndex, offsetNetwork);
      offsetNetwork += layer->width();
      add_const_int("offset_layer_" + layerIndex, offsetLayer);
      offsetLayer += m_network.getBatchSize() * layer->width();
    }
  }
  add_const_int("network_size", offsetNetwork);
  add_const_int("network_ratio", offsetNetwork / m_network.getBatchSize());
  return oss.str();
}

template<class Network>
std::string ShaderGenerator<Network>::genBindingBuffer() {
  std::ostringstream oss;
  oss << '\n';
  const auto add_shader_buffer = [&] (const int binding, const std::string buffer, const std::string name) {
    oss << "layout (binding = " << binding << ") buffer ";
    oss << buffer << " {\n";
    oss << spacePadding(2) << "float " << name << "[];\n";
    oss << "};\n";
  };

  add_shader_buffer(0, "Input", "inNode");
  add_shader_buffer(1, "Output", "outNode");
  add_shader_buffer(2, "NetworkVariables", "network");
  add_shader_buffer(3, "Nodes", "node");
  add_shader_buffer(4, "Entropy", "E");

  return oss.str();
}

template<class Network>
std::string ShaderGenerator<Network>::genPushConstant() {
  std::ostringstream oss;
  oss << '\n';
  oss << "layout (push_constant) uniform TrainingParameter\n" << '{' << '\n';
  oss << spacePadding(2) << "int batch_size;" << '\n';
  oss << spacePadding(2) << "int epoch;" << '\n';
  oss << spacePadding(2) << "float learning_rate;" << '\n';
  oss << spacePadding(2) << "int flagTest;" << '\n';
  oss << "} p;" << '\n';
  return oss.str();
}

template<class Network>
std::string ShaderGenerator<Network>::genMain() {
  std::ostringstream oss;
  oss << '\n' << "void main() {\n";
  oss << "  int gx = int(gl_GlobalInvocationID).x;\n";
  oss << "  if (gx >= p.batch_size) return;\n\n";
  for (const auto& layer: m_network.m_layers) {
    if (!layer->isActivation()) {
      oss << layer->forwardString() << '\n';
    }
  }

  oss << "  if (p.flagTest == 1) return;\n\n";

  for (int i = m_network.m_layers.size(); i > 0; --i) {
    const auto& layer = m_network.m_layers[i-1];
    if (!layer->isActivation()) {
      oss << layer->entropyString() << '\n';
    }
  }

  oss << "  barrier();\n\n";
  addForLoop(oss, "k", "network_ratio + 1", 2);
  addVariable(oss, "int", "offset", "gx + k * num_invocation", 4);
  addVariable(oss, "float", "result", "0.f", 4);
  oss << m_network.m_layers[0]->backwardString();
  oss << m_network.m_layers[1]->backwardString();
  oss << '\n';
  oss << "  }\n";
  oss << "}\n";
  return oss.str();
}

} // namespace sam

#endif // SAM_SHADER_GENERATOR_HPP
