#ifndef SAM_TRAINING_PARAMETER_HPP
#define SAM_TRAINING_PARAMETER_HPP

namespace sam
{

struct TrainingParameter {
  int m_batchSize;
  int m_epoch;
  float m_learningRate;
};

} // namespace sam

#endif // SAM_TRAINING_PARAMETER_HPP
