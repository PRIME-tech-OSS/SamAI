#ifndef SAM_DATA_LOADER_HPP
#define SAM_DATA_LOADER_HPP

#include <vector>
#include <fstream>
#include <sstream>

namespace sam
{

template<typename Tensor>
class DataLoader {
  Tensor m_trainInput;
  Tensor m_trainOutput;
  Tensor m_testInput;
  Tensor m_testOutput;

public:
  DataLoader() {}
  ~DataLoader() {}
  void loadTrainFile(const std::string& path, const int outputOffset);
  void loadTestFile(const std::string& path, const int outputOffset);
  void loadData(Tensor input, Tensor output);
  Tensor trainInput() const;
  Tensor trainOutput() const;
  Tensor testInput() const;
  Tensor testOutput() const;
};

template<typename Tensor>
Tensor DataLoader<Tensor>::trainInput() const {
  return m_trainInput;
}

template<typename Tensor>
Tensor DataLoader<Tensor>::trainOutput() const {
  return m_trainOutput;
}

template<typename Tensor>
Tensor DataLoader<Tensor>::testInput() const {
  return m_testInput;
}

template<typename Tensor>
Tensor DataLoader<Tensor>::testOutput() const {
  return m_testOutput;
}

template<typename Tensor>
void DataLoader<Tensor>::loadTrainFile(const std::string& path, const int outputOffset)
{
  std::ifstream inf(path);

  if (!inf) {
    return;
  }

  std::vector<float> in;
  std::vector<float> out;
  int count = 0;
  while (inf) {
    count++;
    std::string line;
    getline(inf, line);
    std::istringstream iss(line);
    int i = 0;
    for (std::string s; iss >> s; ++i) {
      float value = std::stof(s);
      if (i < outputOffset) {
        in.push_back(value);
      } else {
        out.push_back(value);
      }
    }
  }

  m_trainInput.setValue(in);
  m_trainInput.setShape({count, outputOffset});
  m_trainOutput.setValue(out);
  m_trainOutput.setShape({count});
}

template<typename Tensor>
void DataLoader<Tensor>::loadTestFile(const std::string& path, const int outputOffset)
{
  std::ifstream inf(path);

  if (!inf) {
    return;
  }

  std::vector<float> in;
  std::vector<float> out;
  int count = 0;
  while (inf) {
    count++;
    std::string line;
    getline(inf, line);
    std::istringstream iss(line);
    int i = 0;
    for (std::string s; iss >> s; ++i) {
      float value = std::stof(s);
      if (i < outputOffset) {
        in.push_back(value);
      } else {
        out.push_back(value);
      }
    }
  }

  m_testInput.setValue(in);
  m_testInput.setShape({count, outputOffset});
  m_testOutput.setValue(out);
  m_testOutput.setShape({count});
}

} // namespace sam

#endif // SAM_DATA_LOADER_HPP
