#ifndef SAM_LAYER_HPP
#define SAM_LAYER_HPP

#include "LayerShape.hpp"

namespace sam
{

class Layer {
  bool m_isActivation;
  int m_index;
  LayerShape m_shape;
  bool m_isOutputLayer;
public:
  Layer(int id, bool isActivation = false, bool isOutputLayer = false): m_index(id), m_isActivation(isActivation), m_isOutputLayer(isOutputLayer) {}

  void setShape(int dim, int w, int h, int d);
  bool isActivation() { return m_isActivation; }
  int index() { return m_index; }
  LayerShape shape() { return m_shape; }
  bool isOutputLayer() { return m_isOutputLayer; }
  int dimension() { return m_shape.dimension; }
  int width() { return m_shape.width; }
  int height() { return m_shape.height; }
  int depth() { return m_shape.depth; }
  void setOutputLayer(bool isOutputLayer) {
    m_isOutputLayer = isOutputLayer;
  }

  virtual const std::string forwardString() { return "";};
  virtual const std::string backwardString() { return "";};
  virtual const std::string entropyString() { return "";};
};

void Layer::setShape(int dim, int w, int h, int d) {
  m_shape.dimension = dim;
  m_shape.width = w;
  m_shape.height = h;
  m_shape.depth = d;
}

} // namespace sam

#endif // SAM_LAYER_HPP
