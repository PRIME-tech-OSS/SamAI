#ifndef SAM_TRAINER_HPP
#define SAM_TRAINER_HPP

#include <Saga.h>
#include "ShaderGenerator.hpp"
#include "Utility.hpp"

namespace sam
{

template<class Network>
class Trainer {
	Network m_network;
  std::unique_ptr<saga::SagaDevice> m_device = nullptr;
	saga::video::PushConstantHandle m_constant;
	saga::video::ShaderBufferHandle m_inputBuffer;
	saga::video::ShaderBufferHandle m_outputBuffer;
	saga::video::ShaderBufferHandle m_networkBuffer;
	saga::video::ShaderBufferHandle m_nodeBuffer;
	saga::video::ShaderBufferHandle m_entropyBuffer;
  saga::video::ShaderHandle m_computePipeline;
  int m_groupCount = 1;
 
public:
	Trainer(Network network);
	void init();
	void configure();
	void run();
};

template<class Network>
Trainer<Network>::Trainer(Network network) {
	m_network = network;
  m_device = saga::createDevice(saga::video::E_DRIVER_TYPE::VULKAN, {800, 600}, 16, false, false, false);
}

template<class Network>
void Trainer<Network>::init() {
	
}

template<class Network>
void Trainer<Network>::run() {
  auto& driver = m_device->getVideoDriver();
	auto computeShaderInfo = driver->createShader();
  ShaderGenerator<Network> codeGen {m_network};
  const auto shader = codeGen.run();
  computeShaderInfo.CSource = shader;

  int flagTest = 0;
  auto pushConstantInfo = driver->createPushConstant();
  pushConstantInfo.Size = sizeof(m_network.m_parameter) + sizeof(flagTest);
  m_constant = driver->createResource(std::move(pushConstantInfo));

  auto inputBufferInfo = driver->createShaderBuffer();
  inputBufferInfo.Size = sizeof(float) * (m_network.getTrainInput().getShape())[0] * (m_network.getTrainInput().getShape())[1];
  m_inputBuffer = driver->createResource(std::move(inputBufferInfo));

  auto outputBufferInfo = driver->createShaderBuffer();
  outputBufferInfo.Size = sizeof(float) * m_network.getTrainOutput().getShape()[0];
  m_outputBuffer = driver->createResource(std::move(outputBufferInfo));

  int count = 0;
  int countNodes = 0;
  for (const auto& layer: m_network.getLayer()) {
    auto* castLayer = static_cast<Dense<float>*>(layer);
    count += castLayer->weight().size();
    count += castLayer->bias().size();
    countNodes += castLayer->width();
  }
  auto networkBufferInfo = driver->createShaderBuffer();
  networkBufferInfo.Size = sizeof(float) * count;
  m_networkBuffer = driver->createResource(std::move(networkBufferInfo));

  auto nodeBufferInfo = driver->createShaderBuffer();
  nodeBufferInfo.Size = sizeof(float) * m_network.getTrainInput().getShape()[0] * countNodes;
  m_nodeBuffer = driver->createResource(std::move(nodeBufferInfo));

  auto entropyBufferInfo = driver->createShaderBuffer();
  entropyBufferInfo.Size = sizeof(float) * m_network.getTrainInput().getShape()[0] * countNodes;
  m_entropyBuffer = driver->createResource(std::move(entropyBufferInfo));

  driver->updateShaderBuffer(m_inputBuffer, m_network.getTrainInput().getValue().data());
  driver->updateShaderBuffer(m_outputBuffer, m_network.getTrainOutput().getValue().data());
  count = 0;
  for (const auto& layer: m_network.getLayer()) {
    auto* castLayer = static_cast<Dense<float>*>(layer);
    driver->updateShaderBuffer(m_networkBuffer, castLayer->weight().getValue().data(), count, sizeof(float) * castLayer->weight().size());
    count += sizeof(float) * castLayer->weight().size();
    driver->updateShaderBuffer(m_networkBuffer, castLayer->bias().getValue().data(), count, sizeof(float) * castLayer->bias().size());
    count += sizeof(float) * castLayer->bias().size();
  }

  auto computePipelineInfo = driver->createPipeline();
  computePipelineInfo.Shaders = driver->createResource(std::move(computeShaderInfo));
  m_computePipeline = driver->createResource(std::move(computePipelineInfo));

  int step = 0;
  while (m_device->run()) {
    if (step <= m_network.m_parameter.m_epoch && m_device->isWindowActive()) {
      if (step == m_network.m_parameter.m_epoch) {
        driver->updateShaderBuffer(m_inputBuffer, m_network.getTestInput().getValue().data());
        flagTest = 1;
      }
      driver->begin();
      driver->bindComputePipeline(m_computePipeline);
      driver->updatePushConstant(m_constant, &m_network.m_parameter, 0, sizeof(m_network.m_parameter));
      driver->updatePushConstant(m_constant, &m_network.m_parameter, sizeof(m_network.m_parameter), sizeof(flagTest));
      driver->bindShaderBuffer(m_inputBuffer, 0);
      driver->bindShaderBuffer(m_outputBuffer, 1);
      driver->bindShaderBuffer(m_networkBuffer, 2);
      driver->bindShaderBuffer(m_nodeBuffer, 3);
      driver->bindShaderBuffer(m_entropyBuffer, 4);
      driver->dispatchComputePipeline(m_groupCount, 1, 1);
      driver->end();
      driver->submit();
      driver->present();
      step++;
    } else {
      m_device->yield();
      break;
    }
  }
  // auto* nodeData = (float *) driver->mapBuffer(m_nodeBuffer);
  // std::vector<float> node2 (m_network.getTrainInput().getShape()[0]);
  // for (int i = 0; i < m_network.getTrainInput().getShape()[0]; ++i) {
  //   std::cout << m_network.getTestInput().getValue()[i * 3] << " + ";
  //   std::cout << m_network.getTestInput().getValue()[i * 3 + 1] << " + ";
  //   std::cout << m_network.getTestInput().getValue()[i * 3 + 2] << " = ";
  //   std::cout << *(nodeData + m_network.getTrainInput().getShape()[0] * 4 + i) << std::endl;
  // }
}

} // namespace sam

#endif // SAM_TRAINER_HPP
