#ifndef SAM_EVALUATOR_HPP
#define SAM_EVALUATOR_HPP

namespace sam
{

enum class Evaluator {
  ACCURACY
};

} // namespace sam

#endif // SAM_EVALUATOR_HPP
