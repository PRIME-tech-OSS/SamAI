#ifndef SAM_UTILITY_HPP
#define SAM_UTILITY_HPP

#include <string>
#include <random>

namespace sam
{

std::string spacePadding(int numPad) {
  std::string pad = "";
  for (int i = 0; i < numPad; ++i) {
    pad += ' ';
  }
  return pad;
}

auto addForLoop(std::ostringstream& oss, const std::string cursor, const std::string endValue, const int spacePad) {
  oss << spacePadding(spacePad) << "for (int " << cursor << " = 0; ";
  oss << cursor << " < " << endValue << "; ++" << cursor << ") {\n";
};

auto addVariable(std::ostringstream& oss, const std::string dataType, const std::string name, const std::string value, const int spacePad) {
  oss << spacePadding(spacePad) << dataType << ' ' << name << " = " << value << ";\n";
};

auto addExpression(std::ostringstream& oss, const std::string result, const std::string first, const std::string second, const std::string symbol, const std::string op, const int spacePad) {
  oss << spacePadding(spacePad) << result << ' ' << symbol << ' ' << first;
  if (second != "") {
    oss << ' ' << op << ' ' << second;
  }
  oss << ";\n";
};

void genRandomVector(std::vector<float>& v, float begin, float end) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> disPos(begin, end);
  for (auto& e: v) {
    e = disPos(gen);
  }
}

} // namespace sam

#endif // SAM_UTILITY_HPP
