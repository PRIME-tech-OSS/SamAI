#ifndef SAM_NETWORK_HPP
#define SAM_NETWORK_HPP

#include "Layer.hpp"
#include "Optimizer.hpp"
#include "LossFunction.hpp"
#include "Evaluator.hpp"
#include "TrainingParameter.hpp"

namespace sam
{

template<typename T>
class Network {
  Tensor<T> m_trainInput;
  Tensor<T> m_trainOutput;
  Tensor<T> m_testInput;
  Tensor<T> m_testOutput;

public:
  void setTrainData(const Tensor<T>& x, const Tensor<T>& y);
  void setTestData(const Tensor<T>& x, const Tensor<T>& y);
  void setTrainingParameter(const TrainingParameter& param);
  void addLayer(Layer& layer);
  void train(const TrainingParameter& param);
  int getBatchSize();
  const Tensor<T>& getTrainInput();
  const Tensor<T>& getTrainOutput();
  const Tensor<T>& getTestInput();
  const Tensor<T>& getTestOutput();
  const std::vector<Layer*>& getLayer();
  const std::vector<LayerShape>& getShapeLayers();

public:
  std::vector<Layer*> m_layers;
  TrainingParameter m_parameter;
};

template<typename T>
void Network<T>::addLayer(Layer& layer) {
  m_layers.push_back(&layer);
}

template<typename T>
int Network<T>::getBatchSize() {
  return m_parameter.m_batchSize;
}

template<typename T>
void Network<T>::setTrainingParameter(const TrainingParameter& param) {
  m_parameter = param;
}

template<typename T>
void Network<T>::setTrainData(const Tensor<T>& x, const Tensor<T>& y) {
  m_trainInput = x;
  m_trainOutput = y;
}

template<typename T>
void Network<T>::setTestData(const Tensor<T>& x, const Tensor<T>& y) {
  m_testInput = x;
  m_testOutput = y;
}

template<typename T>
const Tensor<T>& Network<T>::getTrainInput() {
  return m_trainInput;
}

template<typename T>
const Tensor<T>& Network<T>::getTrainOutput() {
  return m_trainOutput;
}

template<typename T>
const Tensor<T>& Network<T>::getTestInput() {
  return m_testInput;
}

template<typename T>
const Tensor<T>& Network<T>::getTestOutput() {
  return m_testOutput;
}

template<typename T>
const std::vector<Layer*>& Network<T>::getLayer() {
  return m_layers;
}

template<typename T>
const std::vector<LayerShape>& Network<T>::getShapeLayers() {
  std::vector<LayerShape> shapes (m_layers.size());
  for (int i = 0; i < m_layers.size(); ++i) {
    shapes.push_back(m_layers[i]->shape());
  }
}

} // namespace sam

#endif // SAM_NETWORK_HPP
