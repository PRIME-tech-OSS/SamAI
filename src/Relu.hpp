#ifndef SAM_RELU_HPP
#define SAM_RELU_HPP

#include "Layer.hpp"
#include "Tensor.hpp"

namespace sam
{

template<typename T>
class Relu: public Layer {
public:
  Relu();
  Relu(int id, bool isActivation = false, bool isOutputLayer = false);
  void forward(const Tensor<T>& in, Tensor<T>& out);
};

template<typename T>
Relu<T>::Relu() {

}

template<typename T>
Relu<T>::Relu(int id, bool isActivation, bool isOutputLayer) : Layer(id, isActivation, isOutputLayer) {}

// template<typename T>
// const std::string Relu<T>::opString(int layerIndex) {
//   std::ostringstream oss;
//   std::string i0 = std::to_string(layerIndex - 1);
//   std::string i1 = std::to_string(layerIndex);
//   std::string nodeVar = "nodeLayer" + i1 + "[gy * shape.layer" + i0 + ".z + gx]";
//   oss << "  " << nodeVar << " = max(" << nodeVar << ", 0);" << '\n';
//   return oss.str();
// }

} // namespace sam

#endif // SAM_RELU_HPP
