#ifndef SAM_LOSS_FUNCTION_HPP
#define SAM_LOSS_FUNCTION_HPP

namespace sam
{

enum class LossFunction {
  SQUARE_ROOT,
  CROSS_ENTROPY
};

} // namespace sam


#endif // SAM_LOSS_FUNCTION_HPP
