#ifndef SAM_CONV_SETTING_HPP
#define SAM_CONV_SETTING_HPP

#include "Tensor.hpp"

namespace sam
{

template<typename T>
class ConvolutionSetting {
  Tensor<T> m_kernel;
  Tensor<T> m_bias;
  int m_stride;
  int m_numFilter;
  T m_padding;
  int m_numPadding;
public:
  void forward(const Tensor<T>& inTensor);
  int kernelWidth();
  int kernelHeight();
  int bias();
  int stride();
  int numFilter();
  int padding();
  int numPadding();
};

} // namespace sam

#endif // SAM_CONV_SETTING_HPP
