#ifndef SAM_CONVOLUTION_HPP
#define SAM_CONVOLUTION_HPP

#include "Layer.hpp"

namespace sam
{

template<typename T>
class Convolution: public Layer {
  Tensor<T> m_kernel;
  Tensor<T> m_bias;
  LayerShape m_shape;
public:
  int m_numFilter;
  int m_strideWidth;
  int m_strideHeight;
  T m_padding;
  int m_paddingWidth;
  int m_paddingHeight;

public:
  void forward(const Tensor<T>& in, const Tensor<T>& out);
};

} // namespace sam

#endif // SAM_CONVOLUTION_HPP
