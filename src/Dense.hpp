#ifndef SAM_DENSE_HPP
#define SAM_DENSE_HPP

#include "Layer.hpp"
#include "Tensor.hpp"
#include "Utility.hpp"

namespace sam
{

template<typename T>
class Dense: public Layer {
  Tensor<T> m_weight;
  Tensor<T> m_bias;
public:
  Dense();
  Dense(int id, bool isActivation = false, bool isOutputLayer = false);
  void init();
  const Tensor<T>& weight();
  const Tensor<T>& bias();
  virtual const std::string forwardString();
  virtual const std::string backwardString();
  virtual const std::string entropyString();
};

template<typename T>
Dense<T>::Dense() {}

template<typename T>
Dense<T>::Dense(int id, bool isActivation, bool isOutputLayer) : Layer(id, isActivation, isOutputLayer) {}

template<typename T>
void Dense<T>::init() {
  m_weight = Tensor<T>(this->width() * this->height());
  m_weight.setShape({this->height(), this->width()});
  m_bias = Tensor<T>(this->width());
  m_bias.setShape({this->width()});
  genRandomVector(m_weight.m_value, -1.0, 1.0);
  genRandomVector(m_bias.m_value, -1.0, 1.0);
}

template<typename T>
const Tensor<T>& Dense<T>::weight() {
  return m_weight;
}

template<typename T>
const Tensor<T>& Dense<T>::bias() {
  return m_bias;
}

template<typename T>
const std::string Dense<T>::forwardString() {
  std::ostringstream oss;
  const std::string id = std::to_string(this->index());
  addForLoop(oss, "j", "width_layer_" + id, 2);
  addVariable(oss, "float", "temp", "0.f", 4);
  addVariable(oss, "const int", "k0", "gx * height_layer_" + id, 4);
  addVariable(oss, "const int", "k1", "j + offset_weight_" + id, 4);
  addForLoop(oss, "i", "height_layer_" + id, 4);
  const std::string node = (this->index() == 1) ? ("inNode[k0 + i]") : ("node[k0 + i]");
  addExpression(oss, "temp", node, "network[k1 + i * width_layer_" + id + "]", "+=", "*", 6);
  oss << "    }\n";
  addExpression(oss, "temp", "network[offset_bias_" + id + " + j]", "", "+=", "", 4);
  addExpression(oss, "node[offset_layer_" + id + " + gx * width_layer_" + id + " + j]", "temp", "", "=", "", 4);
  oss << "  }\n";

  return oss.str();
}

template<typename T>
const std::string Dense<T>::entropyString() {
  std::ostringstream oss;
  const std::string id = std::to_string(this->index());
  const std::string nextId = std::to_string(this->index() + 1);
  if (isOutputLayer()) {
    addVariable(oss, "float", "temp", "0.f", 2);
    addForLoop(oss, "j", "width_layer_" + id, 2);
    addExpression(oss, "temp", "node[offset_layer_" + id + " + gx * width_layer_" + id + " + j]", "outNode[gx * width_layer_" + id + " + j]", "+=", "-", 4);
    oss << "  }\n";
    addExpression(oss, "E[offset_layer_" + id + " + gx * width_layer_" + id + "]", "temp", "p.batch_size", "=", "/", 2);
  } else {
    addForLoop(oss, "i", "height_layer_" + nextId, 2);
    addVariable(oss, "float", "temp", "0.f", 4);
    addForLoop(oss, "j", "width_layer_" + nextId, 4);
    addExpression(oss, "temp", "E[offset_layer_" + nextId + " + gx * width_layer_" + nextId + " + j]",
      "network[offset_weight_" + nextId + " + i * width_layer_" + nextId + " + j]", "+=", "*", 6);
    oss << "    }\n";
    addExpression(oss, "E[offset_layer_" + id + " + gx * width_layer_" + id + " + i]", "temp", "", "=", "", 4);
    oss << "  }\n";
  }
  return oss.str();
}

template<typename T>
const std::string Dense<T>::backwardString() {
  std::ostringstream oss;
  const std::string id = std::to_string(this->index());
  const std::string nextId = std::to_string(this->index() + 1);
  const std::string head = (this->index() == 1) ? "    if " : " else if ";
  oss << head << "(offset < offset_bias_" + id << ") {\n";
  const std::string offset1 = "(offset - offset_weight_" + id + ") / width_layer_" + id;
  const std::string offset2 = "(offset - offset_weight_" + id + ") % width_layer_" + id;
  addVariable(oss, "int", "i", offset1, 6);
  addVariable(oss, "int", "j", offset2, 6);
  addForLoop(oss, "h", "p.batch_size", 6);
  const std::string node = (this->index() == 1) ? ("inNode") : ("node");
  addExpression(oss, "result", node + "[i + h * height_layer_" + id + "]", "E[offset_layer_" + id + " + j + h * width_layer_" + id + "]", "+=", "*", 8);
  oss << "      }\n";
  addExpression(oss, "network[offset]", "p.learning_rate", "result", "-=", "*", 6);
  const std::string tail = isOutputLayer() ? "network_size" : ("offset_weight_" + nextId);
  oss << "    } else if (offset < " << tail << ") {\n";
  addVariable(oss, "int", "i", "offset - offset_bias_" + id, 6);
  addForLoop(oss, "h", "p.batch_size", 6);
  addExpression(oss, "result", "E[offset_layer_" + id + " + i + h * width_layer_" + id + "]", "", "+=", "", 8);
  oss << "      }\n";
  addExpression(oss, "network[offset]", "p.learning_rate", "result", "-=", "*", 6);
  oss << "    }";
  return oss.str();
}

} // namespace sam

#endif // SAM_DENSE_HPP
