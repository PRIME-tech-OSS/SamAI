#ifndef SAM_TENSOR_HPP
#define SAM_TENSOR_HPP

#include <vector>

namespace sam
{

template<typename T>
class Tensor {
public:
  std::vector<T> m_value;
  std::vector<int> m_shape;
public:
  Tensor() {}
  Tensor(int size);
  int dimension();
  void setShape(const std::vector<int>& shape);
  void setValue(const std::vector<T>& value);
  std::vector<int> getShape() const;
  std::vector<T> getValue() const;
  int size() const;
  T getElement(const std::vector<int>& indices);
};

template<typename T>
int Tensor<T>::dimension() {
	return m_shape.size();
}

template<typename T>
Tensor<T>::Tensor(int size) {
	std::vector<T> v (size);
	m_value = v;
}

template<typename T>
void Tensor<T>::setShape(const std::vector<int>& shape) {
	m_shape = shape;
}

template<typename T>
void Tensor<T>::setValue(const std::vector<T>& value) {
	m_value = value;
}

template<typename T>
std::vector<int> Tensor<T>::getShape() const {
	return m_shape;
}

template<typename T>
std::vector<T> Tensor<T>::getValue() const {
	return m_value;
}

template<typename T>
int Tensor<T>::size() const {
	return m_value.size();
}

} // namespace sam

#endif // SAM_TENSOR_HPP
