#ifndef SAM_OPTIMIZER_HPP
#define SAM_OPTIMIZER_HPP

namespace sam
{

enum class Optimizer {
  ADAM,
  SGD
};

} // namespace sam

#endif // SAM_OPTIMIZER_HPP
