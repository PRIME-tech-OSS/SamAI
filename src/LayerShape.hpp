#ifndef SAM_LAYER_SHAPE_HPP
#define SAM_LAYER_SHAPE_HPP

namespace sam
{

struct LayerShape
{
  int dimension;
  int width;
  int height;
  int depth;
};

} // namespace sam

#endif // SAM_LAYER_SHAPE_HPP
