#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../src/SamAI.hpp"

using namespace sam;

TEST_CASE() {
  Network<float> network;
  Dense<float> layer1 {1};
  layer1.setShape(2, 4, 3, 1);
  Dense<float> layer2 {2};
  layer2.setShape(2, 1, 4, 1);
  layer2.setOutputLayer(true);
  network.addLayer(layer1);
  network.addLayer(layer2);
  TrainingParameter param {512, 100, 0.01};
  network.setTrainingParameter(param);
  ShaderGenerator<Network<float>> gen {network};
  // Test constant variables
  const auto const_shader = R"(
const int num_data = 512;
const int num_invocation = 512;
const int width_layer_1 = 4;
const int height_layer_1 = 3;
const int offset_weight_1 = 0;
const int offset_bias_1 = 12;
const int offset_layer_1 = 0;
const int width_layer_2 = 1;
const int height_layer_2 = 4;
const int offset_weight_2 = 16;
const int offset_bias_2 = 20;
const int offset_layer_2 = 2048;
const int network_size = 21;
const int network_ratio = 0;
)";
  auto const_output = gen.genConstantVariable();
  REQUIRE(const_output == const_shader);
  // Test gen binding buffer
  const auto buffer_shader = R"(
layout (binding = 0) buffer Input {
  float inNode[];
};
layout (binding = 1) buffer Output {
  float outNode[];
};
layout (binding = 2) buffer NetworkVariables {
  float network[];
};
layout (binding = 3) buffer Nodes {
  float node[];
};
layout (binding = 4) buffer Entropy {
  float E[];
};
)";
  auto gen_buffer_shader = gen.genBindingBuffer();

  REQUIRE(gen_buffer_shader == buffer_shader);

  const auto push_constant_shader = R"(
layout (push_constant) uniform TrainingParameter
{
  int batch_size;
  int epoch;
  float learning_rate;
  int flagTest;
} p;
)";
  auto gen_push_constant = gen.genPushConstant();

  REQUIRE(gen_push_constant == push_constant_shader);

  const auto main_shader = R"(
void main() {
  int gx = int(gl_GlobalInvocationID).x;
  if (gx >= p.batch_size) return;

  for (int j = 0; j < width_layer_1; ++j) {
    float temp = 0.f;
    const int k0 = gx * height_layer_1;
    const int k1 = j + offset_weight_1;
    for (int i = 0; i < height_layer_1; ++i) {
      temp += inNode[k0 + i] * network[k1 + i * width_layer_1];
    }
    temp += network[offset_bias_1 + j];
    node[offset_layer_1 + gx * width_layer_1 + j] = temp;
  }

  for (int j = 0; j < width_layer_2; ++j) {
    float temp = 0.f;
    const int k0 = gx * height_layer_2;
    const int k1 = j + offset_weight_2;
    for (int i = 0; i < height_layer_2; ++i) {
      temp += node[k0 + i] * network[k1 + i * width_layer_2];
    }
    temp += network[offset_bias_2 + j];
    node[offset_layer_2 + gx * width_layer_2 + j] = temp;
  }

  if (p.flagTest == 1) return;

  float temp = 0.f;
  for (int j = 0; j < width_layer_2; ++j) {
    temp += node[offset_layer_2 + gx * width_layer_2 + j] - outNode[gx * width_layer_2 + j];
  }
  E[offset_layer_2 + gx * width_layer_2] = temp / p.batch_size;

  for (int i = 0; i < height_layer_2; ++i) {
    float temp = 0.f;
    for (int j = 0; j < width_layer_2; ++j) {
      temp += E[offset_layer_2 + gx * width_layer_2 + j] * network[offset_weight_2 + i * width_layer_2 + j];
    }
    E[offset_layer_1 + gx * width_layer_1 + i] = temp;
  }

  barrier();

  for (int k = 0; k < network_ratio + 1; ++k) {
    int offset = gx + k * num_invocation;
    float result = 0.f;
    if (offset < offset_bias_1) {
      int i = (offset - offset_weight_1) / width_layer_1;
      int j = (offset - offset_weight_1) % width_layer_1;
      for (int h = 0; h < p.batch_size; ++h) {
        result += inNode[i + h * height_layer_1] * E[offset_layer_1 + j + h * width_layer_1];
      }
      network[offset] -= p.learning_rate * result;
    } else if (offset < offset_weight_2) {
      int i = offset - offset_bias_1;
      for (int h = 0; h < p.batch_size; ++h) {
        result += E[offset_layer_1 + i + h * width_layer_1];
      }
      network[offset] -= p.learning_rate * result;
    } else if (offset < offset_bias_2) {
      int i = (offset - offset_weight_2) / width_layer_2;
      int j = (offset - offset_weight_2) % width_layer_2;
      for (int h = 0; h < p.batch_size; ++h) {
        result += node[i + h * height_layer_2] * E[offset_layer_2 + j + h * width_layer_2];
      }
      network[offset] -= p.learning_rate * result;
    } else if (offset < network_size) {
      int i = offset - offset_bias_2;
      for (int h = 0; h < p.batch_size; ++h) {
        result += E[offset_layer_2 + i + h * width_layer_2];
      }
      network[offset] -= p.learning_rate * result;
    }
  }
}
)";
  auto gen_main = gen.genMain();

  REQUIRE(gen_main == main_shader);
}
