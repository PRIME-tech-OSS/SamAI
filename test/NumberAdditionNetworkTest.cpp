#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <sstream>
#include <random>
#include <Saga.h>
#include <iostream>

#include "../src/SamAI.hpp"

using namespace sam;
using namespace saga;
using namespace core;
using namespace video;

TEST_CASE() {
  // Network initialization
  DataLoader<Tensor<float>> loader;
  loader.loadTrainFile("../data/number_addition_train.txt", 3);
  loader.loadTestFile("../data/number_addition_test.txt", 3);
  Network<float> network;
  network.setTrainData(loader.trainInput(), loader.trainOutput());
  network.setTestData(loader.testInput(), loader.testOutput());
  Dense<float> layer1 {1};
  layer1.setShape(2, 4, 3, 1);
  layer1.init();
  Dense<float> layer2 {2};
  layer2.setShape(2, 1, 4, 1);
  layer2.setOutputLayer(true);
  layer2.init();
  network.addLayer(layer1);
  network.addLayer(layer2);
  TrainingParameter param {8, 100, 0.1};
  network.setTrainingParameter(param);

  Trainer<Network<float>> trainer {network};
  trainer.run();
}
