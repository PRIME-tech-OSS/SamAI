# SamAI Project by [InnerPiece](https://innerpiece.io/)

**Sam (Saga3D Artificial Intelligent & Machine Learning) is an AI project for GPU, based on InnerPiece's Saga3D. It provides ML algorithms running on Vulkan backend. Sam can be integrated into a 3D Environment (Game, Simulation, ...).**

Origin of this project: [InnerPiece Founder's blog post](https://blog.ntmanh.net/vulkan-for-ai-ml/).

Similar projects:
- Microsoft's [DirectML](https://devblogs.microsoft.com/directx/directml-at-gdc-2019/)
- Alibaba's [MNN](https://github.com/alibaba/MNN)
- Tencent's [ncnn](https://github.com/Tencent/ncnn)
- Vulkan [ML](https://www.phoronix.com/scan.php?page=news_item&px=SIGGRAPH-2019-Vulkan-ML)

## SamAI project members
- Project Lead: Manh Nguyen Tien <br />
- Core Developer: Anh Phan Tuan <br />
Published AI Researchs: [Google Scholar](https://scholar.google.com/citations?user=bw48bNQAAAAJ&hl=en) <br />
AI researchs under review: updating

## Progress tracking
- See [milestones](https://gitlab.com/InnerPieceOSS/SamAI/-/milestones).
- First we'll publish a `white paper` to show technical aspects of project. This process will involve several AI teams and researchers in Vietnam to make sure we will design a good project for people to use.
- Participate in development discussions on our [mailing list](https://groups.google.com/forum/#!forum/innerpiece_oss) or Discord chat [server](https://discord.gg/QdY7tuJ) (channel `#sam_ai)`.
